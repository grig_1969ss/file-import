<?php
/**
 * Created by PhpStorm.
 * User: Aram
 * Date: 1/23/2018
 * Time: 4:18 PM
 */

namespace app\controllers;

use app\models\Files;
use Faker\Provider\File;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;

class AjaxController extends Controller
{
    public $enableCsrfValidation=false;

    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    public function actionCall(){
        $params = \Yii::$app->request->post();

        $action = ArrayHelper::getValue($params,'action');
        try{
            if(empty($action)){
                throw new Exception('No action');
            }
            $data = $this->$action($params);
            return [
                'respcode'=>1,
                'respmess'=>'Ok',
                'data'=> $data
            ];
        }catch (\Exception $e){
            return [
                'respcode'=>0,
                'respmess'=>$e->getMessage(),
				'data'=>$e->getTraceAsString()
            ];
        }
    }

    public function fileUpload($params){
        if(isset($_FILES["file"])){
            $name = $_FILES["file"]["name"];
            $tmp_name = $_FILES['file']['tmp_name'];
            $store_id = $params['shop'];
            if (isset ($name)) {
                if (!empty($name)) {
                    $file = new \app\models\File();
                    $file_path = $file->checkAndUpload($tmp_name,$name);
                    $file_dir = $file->getFileDir($file_path);
                    $file_ext = $file->splitFileNameToExt($file_path);
                    $file_model = new Files(['path'=>$file_path]);
                    $file_model->save();
                    if($file_ext == 'csv'){
                        if ($file_dir == 'uploads'){
                            $data = $file->getCsvFileData($file_path,$file_model->id,$store_id,false);
                        }else{
                            $data = $file->getCsvFileData($file_path,$file_model->id,$store_id,true);
                        }
                    }else{
                        if ($file_dir == 'uploads'){
                            $data = $file->getXlsxFileData($file_path,$file_model->id,$store_id,false);
                        }else{
                            $data = $file->getXlsxFileData($file_path,$file_model->id,$store_id,true);
                        }
                    }

                    $datas=array_chunk($data,10);

                    if(count($datas[0][0]) != 4){
                        $columns =  [
                            'store_id',
                            'file_id',
                            'title',
                            'upc',
                            'price',
                        ];
                    }else{
                        $columns =  [
                            'store_id',
                            'file_id',
                            'upc',
                            'price',
                        ];
                    }
                    foreach ($datas as $data){
                        Yii::$app->db->createCommand()->batchInsert(
                            'store_products',
                            $columns,
                            $data
                        )->execute();
                    }
                } else {
                    echo 'please choose a file';
                }
            }
            else{
                echo "name not set";
            }
        }
        else echo "FILES not set!";
        return 'asd';
    }
}