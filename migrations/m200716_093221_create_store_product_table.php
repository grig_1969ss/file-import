<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%store_prducts}}`.
 */
class m200716_093221_create_store_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%store_products}}', [
            'id' => $this->primaryKey(),
            'store_id' => $this->integer()->notNull(),
            'file_id' => $this->integer()->notNull(),
            'upc' => $this->integer(),
            'title' => $this->string(),
            'price' => $this->float(),
            'created_at' => $this->dateTime(),
        ]);


        // add foreign key for table `stores`
        $this->addForeignKey(
            'fk-store_products-store_id',
            'store_products',
            'store_id',
            'stores',
            'id',
            'CASCADE'
        );

        // add foreign key for table `files`
        $this->addForeignKey(
            'fk-store_products-file_id',
            'store_products',
            'file_id',
            'files',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%store_products}}');
    }
}
