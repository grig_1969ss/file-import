var AJAX_URL;

function fileUpload(e) {

    let formData = new FormData($('form')[0]);
    formData.append("action", "fileUpload");

    if($('#shop').val() != ""){
        $.ajax({
            url: AJAX_URL,  //Server script to process data
            type: 'POST',

            // Form data
            data: formData,

            success: function(res) {
                // console.log(res);
                window.location.href = "http://store-file.im/files/index";
            },

            error: function(){
                alert('ERROR at PHP side!!');
            },

            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false
        });
    }


}

function shopField(e) {

    if($(e).val() != "" && $('#file').val()){
        $('#file').trigger('change');
    }

}