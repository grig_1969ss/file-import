<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost:8111;dbname=store_file',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
