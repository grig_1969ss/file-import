<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StoreProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'store_id')->widget(Select2::classname(), [
        'data' => \app\models\Store::getDropdownDatas(),
        'options' => ['placeholder' => 'Select a shop ...'],
    ])->label('Shops'); ?>

    <?= $form->field($model, 'upc')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
