<?php

use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StoreProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-product-form">
    <form  method="post" enctype="multipart/form-data">
        <label class="control-label">Shops</label>

        <?=Select2::widget([
            'name' => 'shop',
            'data' => \app\models\Store::getDropdownDatas(),
            'options' => [
                'placeholder' => 'Select shop ...',
                'id' => 'shop',
                'onchange'=>'shopField(this)',
                'multiple' => false
            ],
        ]);?>
        <br>
        <?=
        FileInput::widget([
            'name' => 'file',
            'language' => 'ru',
            'options' => ['multiple' => false,'data-shop'=>'#shop','id'=>'file','onchange'=>'fileUpload(this)'],
            'pluginOptions' => ['previewFileType' => 'any', 'uploadUrl' => Url::to(['/store-product/form-file']),]
        ]);
        ?>
    </form>


</div>