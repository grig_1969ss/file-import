<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\FilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="files-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Files', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            [
                'attribute'   =>'path',
                'label'=>'File',
                'value' => function ($model)
                {
                    return basename($model->path);
                },
            ]  ,
            [
                'attribute'   =>'store',
                'label'=>'Shop',
                'value' => function ($model)
                {
                    return $model->storeProduct?$model->storeProduct[0]->store->title:basename($model->path);
                },
            ]  ,
            [
                'attribute'   =>'count lines',
                'label'=>'count lines',
                'value' => function ($model)
                {
                    if(count($model->storeProduct)){
                        return count($model->storeProduct);
                    }
                    return 'oshibechnie';
                },
            ]  ,

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
