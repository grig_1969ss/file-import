<?php


namespace app\models;


class File
{
    /**
     * @var File file attribute
     */
    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['file'], 'file','extensions' => 'jpg, png',],
        ];
    }

    public  function splitFileNameToExt($file_name){
        $name = basename($file_name);
        $ext = explode('.', $name);
        return end($ext);
    }

    public  function getFileDir($path){
        $ext = explode('/', $path);
        return current($ext);
    }

    public  function checkAndUpload($file_tmp,$name){
        $tmp_handle = fopen($file_tmp, 'r+');

        rewind($tmp_handle);
        $file_contents = stream_get_contents($tmp_handle);
        if (strpos($file_contents, 'upc') !== false) {
            $location = 'uploads/';
        }else{
            $location = 'error_uploads/';
        }

        $path = $location.time().'_'.$name;
        if  (move_uploaded_file($file_tmp, $path)){
            return $path;
        }
    }
    public  function getCsvFileData($file_path,$file_id,$store_id,$error){
        $data = [];
        $i = 0;
        $title = false;
        $handle = fopen($file_path, "r");
        while (($fileop = fgetcsv($handle, 1000, ",")) !== false)
        {
            $i++;

            if(!is_numeric($fileop[1])){
                if (in_array("title", $fileop)) {
                    $title = true;
                }
                continue;
            }
            if(!$error){
                if ($title) {
                    $data[$i]['store_id'] = $store_id;
                    $data[$i]['file_id'] = $file_id;
                    $data[$i]['title'] = $fileop[0];
                    $data[$i]['upc'] = $fileop[1];
                    $data[$i]['price'] = $fileop[2];
                }else{
                    $data[$i]['store_id'] = $store_id;
                    $data[$i]['file_id'] = $file_id;
                    $data[$i]['upc'] = $fileop[0];
                    $data[$i]['price'] = $fileop[1];
                }
            }else{
              $data = [];
            }
        }
        return $data;
    }
    public function getXlsxFileData($file_path,$file_id,$store_id,$error){
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($file_path);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file_path);
        }catch (\yii\db\Exception $e){
            die('error');
        }
        $title = false;
        $data = [];
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        for ($row = 1;$row <=$highestRow;$row++){
            $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null,true,false);
            if($row==1){
                if (in_array("title", $rowData[0])) {
                    $title = true;
                }
                continue;
            }
            if(!$error){
                $data[$row]['store_id,'] = $store_id;
                $data[$row]['file_id,'] = $file_id;
                if ($title) {
                    $data[$row]['title'] = $rowData[0][0];
                    $data[$row]['upc'] = $rowData[0][1];
                    $data[$row]['price'] = $rowData[0][2];
                }else{
                    $data[$row]['upc'] = $rowData[0][0];
                    $data[$row]['price'] = $rowData[0][1];
                }

            }else{
                $data = [];
            }

        }
        return $data;
    }
}