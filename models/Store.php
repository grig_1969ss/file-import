<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "stores".
 *
 * @property int $id
 * @property string|null $title
 *
 * @property StoreProduct[] $storeProduct
 */
class Store extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * Gets query for [[StoreProducts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStoreProduct()
    {
        return $this->hasMany(StoreProduct::className(), ['store_id' => 'id']);
    }

    public static function getDropdownDatas()
    {
        return ArrayHelper::map(self::find()->all(),'id','title');
    }
}
